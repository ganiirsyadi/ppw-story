// FIX Unaccurate VH in mobile browser

let vh = window.innerHeight * 0.01
document.documentElement.style.setProperty('--vh', `${vh}px`)

// DOM SELECTION

const breakPoint = document.querySelector(".point")
const smallText = document.querySelector(".small-text")
const title = document.querySelectorAll("header h1")
const header = document.querySelector("header")
const box = document.querySelectorAll(".box")
const wrapper = document.querySelector(".wrapper")

///////////////////////////////////////////////////

//INTERSECTION OBSERVER METHOD

const headerOptions = {};
const headerObserver = new IntersectionObserver
(function (
    entries, 
    headerObserver
    ) {  
        entries.forEach(entry => {
            if (!entry.isIntersecting) {
                header.style.backgroundColor = "#000"
                smallText.style.color = "white"
                title.forEach(el => el.style.color = "white")
                box.forEach(el => el.style.backgroundColor = "#131313")
            } else {
                header.style.backgroundColor = "#fff"
                smallText.style.color = "black"
                title.forEach(el => el.style.color = "black")
                box.forEach(el => el.style.backgroundColor = "#000")
            }
        });
    },
    headerOptions)

headerObserver.observe(breakPoint)

/////////////////////////////////////////////////////////////////////

// Custom Event Listener function

function boxIn(event, element, backgroundColor) {   
    element.addEventListener(event, function () {  
        this.style.backgroundColor = "rgba(255,255,255,0.1)";
        this.children[1].style.opacity = "1";
        this.children[1].style.transform = "scale(.9)";
        this.children[0].style.opacity = "0";
        header.style.backgroundColor = backgroundColor;
        wrapper.style.backgroundColor = backgroundColor;
        
    })   
}

function boxOut(event, element) {   
    element.addEventListener(event, function (e) {  
        this.style.backgroundColor = "#131313";
        this.children[0].style.opacity = "1";
        this.children[1].style.opacity = "0";
        this.children[1].style.transform = "scale(1)";
        if (header.getBoundingClientRect().bottom < window.innerHeight) {
            header.style.backgroundColor = "black";
        } else {
            header.style.backgroundColor = "white";
        }
        wrapper.style.backgroundColor = "black";
        
    })   
}

// Calling custom event listener function

boxIn("mouseover",box[0], "#EF3F3F")
boxOut("mouseout",box[0])
boxIn("touchstart",box[0], "#EF3F3F")
boxOut("touchend",box[0])

boxIn("mouseover",box[1], "#4281A4")
boxOut("mouseout",box[1])
boxIn("touchstart",box[1], "#4281A4")
boxOut("touchend",box[1])


boxIn("mouseover",box[2], "#48A9A6")
boxOut("mouseout",box[2])
boxIn("touchstart",box[2], "#48A9A6")
boxOut("touchend",box[2])


boxIn("mouseover",box[3], "#08605F")
boxOut("mouseout",box[3])
boxIn("touchstart",box[3], "#08605F")
boxOut("touchend",box[3])


boxIn("mouseover",box[4], "#F1935C")
boxOut("mouseout",box[4])
boxIn("touchstart",box[4], "#F1935C")
boxOut("touchend",box[4])


boxIn("mouseover",box[5], "#444444")
boxOut("mouseout",box[5])
boxIn("touchstart",box[5], "#444444")
boxOut("touchend",box[5])

// Code above was made by me with some research from developer.mozilla.org


