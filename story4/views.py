from django.shortcuts import render
import datetime

# Create your views here.

def index(request):
    return render(request, 'story4/index.html')
def more(request):
    return render(request, 'story4/more.html')
def timeBlank(request):
    time = datetime.datetime.now()
    now = {
        "now" : time.strftime("%X")
    }
    return render(request, 'story4/time.html', context=now)
def time(request,gmt):
    time = datetime.datetime.now()
    gmt = int(gmt)
    hour = (time.hour + gmt) % 24
    time = time.replace(hour=hour)
    now = {
        "now" : time.strftime("%X")
    }
    return render(request, 'story4/time.html', context=now)
